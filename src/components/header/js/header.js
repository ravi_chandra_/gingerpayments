(function (angular) {
    var templateUrl = 'components/header/html/header.html';

    angular.module('payments.header', [
        templateUrl
    ])
    .value('paymentsHeader')
    .component('paymentsHeader', {
        templateUrl: templateUrl
    });
})(angular);
