(function (angular) {
    var templateUrl = 'components/footer/html/footer.html';

    angular.module('payments.footer', [
        templateUrl
    ])
    .value('$routerRootComponent', 'paymentsFooter')
    .component('paymentsFooter', {
        templateUrl: templateUrl
    });
})(angular);
