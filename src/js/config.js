(function (angular) {
    'use strict';

    function config($locationProvider) {
        $locationProvider.html5Mode(true);
    }
    angular.module('app.config', [
        'ngComponentRouter',
        'payments.header',
        'payments.home',
        'payments.footer'
    ])
    .config(config)
    .value('$routerRootComponent', 'paymentsView')
    .component('paymentsView', {
        template:
            '<payments-header></payments-header>'+
            '<div class="container"><ng-outlet></ng-outlet></div>',
        $routeConfig: [
            {
                path: '/',
                name: 'Home',
                component: 'paymentsHome',
                useAsDefault: true
            }
        ]
    });
})(angular);
