(function (angular) {
    angular.module('payments', [
        'app.config',
        'app.run',
        'app.pages'
    ]);

    angular.module('app.pages', [
    ]);
})(angular);
