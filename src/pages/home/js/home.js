(function (angular) {
    var moduleName = 'payments.home';
    var componentName = 'paymentsHome';
    var templateUrl = 'pages/home/html/home.html';

    function PaymentsHomeController($filter, paymentsService) {
        var $ctrl = this;
        resetFilter();
        $ctrl.filterOptions = [
            {
                name:'creditcard',
                value:'creditcard'
            },
            {
                name:'bank-transfer',
                value:'bank-transfer'
            },
            {
                name:'ideal',
                value:'ideal'
            }
        ];

        $ctrl.paymentMethods = [
            {
                name:'creditcard',
                value:'creditcard'
            },
            {
                name:'ideal',
                value:'ideal'
            },
            {
                name:'bank-transfer',
                value:'bank-transfer'
            }
        ];

        $ctrl.currencys = [
            {
                name:'AUD',
                value:'AUD'
            },
            {
                name:'USD',
                value:'USD'
            },
            {
                name:'GBP',
                value:'GBP'
            }
        ];

        $ctrl.merchants = [
            {
                name:'Bookshop',
                value:'Bookshop'
            },
            {
                name:'Bookshop',
                value:'Bookshop'
            },
            {
                name:'Ginger',
                value:'Ginger'
            },
            {
                name:'Ginger Ale',
                value:'Ginger Ale'
            },
            {
                name:'De online drogist',
                value:'De online drogist'
            },
            {
                name:'Webshop Yoda',
                value:'Webshop Yoda'
            }
        ];

        function resetFilter () {
            $ctrl.isFilterApplied = false;
            $ctrl.selectedOption = '';
        }

        $ctrl.getTop20Payments = function (){
            $ctrl.payments = null;
            $ctrl.showPaymentForm = false;
            resetFilter();
            paymentsService.getPayments().then(function (payments) { //Promis and success call back
                var topPayMents = $filter('orderBy')(payments, 'amount', true);
                $ctrl.payments = $filter('limitTo')(topPayMents, 20, 0);
            }, function (error) { //Error call back
                $ctrl.payments = error;
            });
        }

        $ctrl.getPaymentsByMerchant = function (merchant){
            $ctrl.payments = null;
            $ctrl.showPaymentForm = false;
            resetFilter();
            paymentsService.getPayments().then(function (payments) { //Promise and success call back
                $ctrl.payments = payments.filter(function( payment){
                    return  payment.merchant === merchant;
                });
            }, function (error) { //Error call back
                $ctrl.payments = error;
            });
        }

        $ctrl.filterByPaymentMethod = function (option) {
            if(!$ctrl.isFilterApplied) {
                $ctrl.originalPayments = $ctrl.payments;
            }
            var filterPayments = $ctrl.originalPayments.filter(function( payment){
                return  payment.method === option.value;
            });

            $ctrl.payments = filterPayments;
            $ctrl.isFilterApplied = true;
        }

        $ctrl.togglePayment = function () {
            $ctrl.showPaymentForm = !$ctrl.showPaymentForm;
        }

        $ctrl.addPayment = function (payment) {
            var paymentDetails = {
                "id": 11,
                "method": payment.method.value,
                "amount": payment.amount,
                "currency": payment.currency.value,
                "created": new Date(),
                "status": "accepted",
                "merchant": payment.merchant.value
            }
            paymentsService.addPayments(paymentDetails).then(function (payments) {
                alert('Payment successfull!');
            }, function (error) {
                $ctrl.paymentSuccess = false;
            });
        }
    }
    PaymentsHomeController.$inject = [
        '$filter',
        'paymentsService'
    ];

    function PaymentsService($http, $q) {
        function getPayments(){
            return $http
                .get('http://localhost:3000/payments')
                .then(function (res) {
                    if (res.data) {
                        return res.data;
                    }
                    return $q.reject(res);
                });
        }

        function addPayments(pay){
            return $http
                .post('http://localhost:3000/payments', pay)
                .then(function (res) {
                    if (res.data) {
                        return res.data;
                    }
                    return $q.reject(res);
                });
        }

        return {
            getPayments: getPayments,
            addPayments: addPayments
        };
    }

    PaymentsService.$inject = [
        '$http',
        '$q'
    ];

    angular.module(moduleName, [
        templateUrl
    ])
    .value('$routerRootComponent', 'paymentsHome')
    .component(componentName, {
        templateUrl: templateUrl,
        controller: PaymentsHomeController
    })
    .service('paymentsService', PaymentsService);
})(angular);
