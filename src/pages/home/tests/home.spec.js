describe('home', function () {
    'use strict';

    var paymentsService;
    var element;
    var $scope;
    var $q;

    beforeEach(module('payments.home'));

    beforeEach(inject(function ($rootScope, $compile, _$q_, _paymentsService_) {
        $scope = $rootScope.$new();
        $q = _$q_;
        paymentsService = _paymentsService_;
        element = $compile(angular.element('<payments-home></payments-home>'))($scope);
    }));

    xit('should get top 20 payments order by ammount', function () {

        var mockData = {
            "payments": [
                {
                    "id": 17,
                    "amount": 17
                },
                {
                    "id": 16,
                    "amount": 16
                },
                {
                    "id": 10,
                    "amount": 10
                },
                {
                    "id": 4,
                    "amount": 4
                },
                {
                    "id": 5,
                    "amount": 5
                },
                {
                    "id": 6,
                    "amount": 6
                },
                {
                    "id": 7,
                    "amount": 7
                },
                {
                    "id": 8,
                    "amount": 8
                },
                {
                    "id": 9,
                    "amount": 9
                },
                {
                    "id": 3,
                    "amount": 3
                },
                {
                    "id": 11,
                    "amount": 11
                },
                {
                    "id": 12,
                    "amount": 12
                },
                {
                    "id": 13,
                    "amount": 13
                },
                {
                    "id": 14,
                    "amount": 14
                },
                {
                    "id": 15,
                    "amount": 15
                },
                {
                    "id": 1,
                    "amount": 1
                },
                {
                    "id": 2,
                    "amount": 2
                },
                {
                    "id": 18,
                    "amount": 18
                },
                {
                    "id": 19,
                    "amount": 19
                },
                {
                    "id": 20,
                    "amount": 20
                },
                {
                    "id": 21,
                    "amount": 21
                }
            ]
        };
        var expectedResult = {
            "payments": [
                {
                    "id": 21,
                    "amount": 21
                },
                {
                    "id": 20,
                    "amount": 20
                },
                {
                    "id": 19,
                    "amount": 19
                },
                {
                    "id": 18,
                    "amount": 18
                },
                {
                    "id": 17,
                    "amount": 17
                },
                {
                    "id": 16,
                    "amount": 16
                },
                {
                    "id": 15,
                    "amount": 15
                },
                {
                    "id": 14,
                    "amount": 14
                },
                {
                    "id": 13,
                    "amount": 13
                },
                {
                    "id": 12,
                    "amount": 12
                },
                {
                    "id": 11,
                    "amount": 11
                },
                {
                    "id": 10,
                    "amount": 10
                },
                {
                    "id": 9,
                    "amount": 9
                },
                {
                    "id": 8,
                    "amount": 8
                },
                {
                    "id": 7,
                    "amount": 7
                },
                {
                    "id": 6,
                    "amount": 6
                },
                {
                    "id": 5,
                    "amount": 5
                },
                {
                    "id": 4,
                    "amount": 4
                },
                {
                    "id": 3,
                    "amount": 3
                },
                {
                    "id": 2,
                    "amount": 2
                }
            ]
        };

        spyOn(paymentsService, 'getPayments').and.returnValue($q.when(mockData.payments));
        $scope.$digest();
        var ctrl = element.isolateScope().$ctrl;
        ctrl.getTop20Payments();

        console.log(ctrl.payments);

        expect(paymentsService.getPayments).toHaveBeenCalled();
        expect(angular.equals(ctrl.payments, expectedResult.payments)).toBe(true);
    });
});
